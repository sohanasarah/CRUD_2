<?php
namespace App\Bitm\SEIP139942\Mobile;
class Mobile{
    public $id="";
    public $title="";
    public $created="";
    public $modified="";
    public $created_by="";
    public $deleted_at="";

    public function __construct($data="Default")
    {
        echo "I am constructing";
        echo "<br>";
        $this->title=$data;
    }

    public function prepare($data=""){
        if(array_key_exists("mobile", $data))
        {
            $this->title=$data['mobile'];
        }
    }

    public function index(){
        echo "I am listing data";

    }
    public function create(){
        echo "I am creating form";

    }
    public function store(){
        echo "I am storing data";

    }
    public function edit(){
        echo "I am editing form";

    }
    public function update(){
        echo "I am updating data";

    }
    public function delete(){
        echo "I delete data";

    }


}
//$obj= new Mobile();
//$obj->index();