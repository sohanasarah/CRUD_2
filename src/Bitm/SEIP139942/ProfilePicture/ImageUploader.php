<?php
namespace App\Bitm\SEIP139942\ProfilePicture;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;

class ImageUploader{
    public $name="";
    public $image_name="";
    public $id="";
    public $conn;
    public $deleted_at;

    public function __construct(){
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("database connection failed");

    }

    public function prepare($data=""){
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return  $this;


    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`profilepicture` (`name`, `images`) VALUES ('".$this->name."', '".$this->image_name."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }
    public function index(){
        $_allinfo=array();
        $query="SELECT * FROM `atomicprojectb22`.`profilepicture` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allinfo[] = $row;
        }

        return $_allinfo;

    }

    public function view(){
        $query="SELECT * FROM `atomicprojectb22`.`profilepicture` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = '" . $this->name . "', `images` = '" . $this->image_name . "' WHERE `profilepicture`.`id` =" . $this->id;
        }
        else {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = '" . $this->name ."' WHERE `profilepicture`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\"><strong>Success!</strong> Data has been updated  successfully.</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`profilepicture`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
  <div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
  </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
  <div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
  </div>");
            Utility::redirect("index.php");
        }

    }

    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` = '".$this->deleted_at."' WHERE `profilepicture`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashed=array();
        $query="SELECT * FROM `profilepicture`WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashed[]=$row;
        }

        return $_trashed;


    }
    public function recover(){
        $query="UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` = NULL  WHERE `profilepicture`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb22`.`profilepicture`  WHERE `profilepicture`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }





}