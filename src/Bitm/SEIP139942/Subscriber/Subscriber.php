<?php
namespace App\Bitm\SEIP139942\Subscriber;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;

class Subscriber{
    public $email="";
    public $id="";
    public $conn;
    public $deleted_at;

    public function prepare($data="")
    {
        if(array_key_exists("email", $data))
        {
            $this->email=$data['email'];
        }
        if(array_key_exists("id", $data))
        {
            $this->id=$data['id'];
        }
    }
    public function __construct()
    {

        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("database connection failed");
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`email` (`email`) VALUES ('".$this->email."')";
        $result=mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\"><strong>Stored!</strong> Data has been stored successfully.</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Data has not been stored. Try Again</div>");
            Utility::redirect('index.php');

        }


    }
    public function index(){
        $_allEmail=array();
        $query="SELECT * FROM `email` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allEmail[]=$row;
        }

        return $_allEmail;


    }
    public function view(){
        $query="SELECT * FROM `email` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `atomicprojectb22`.`email` SET `email` = '".$this->email."' WHERE `email`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\"><strong>Deleted!</strong> Data has been updated successfully.</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Data has not been updated successfully.</div>");
            Utility::redirect('index.php');

        }

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\"><strong>Deleted!</strong> Data has been deleted successfully.</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\"> <strong>Error!</strong> Data has not been deleted successfully.</div>");
            Utility::redirect('index.php');

        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb22`.`email` SET `deleted_at` = '".$this->deleted_at."' WHERE `email`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\"><strong>Trashed!</strong> Data has been trashed successfully.</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Data has not been trashed successfully.</div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashedEmail=array();
        $query="SELECT * FROM `email` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashedEmail[]=$row;
        }

        return $_trashedEmail;


    }

    public function recover(){
        $query="UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL WHERE `email`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\"><strong>Trashed!</strong> Data has been recovered successfully.</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Data has not been recovered successfully.</div>");
            Utility::redirect('index.php');
        }

    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){   //multiple select
            $IDs= implode(",",$idS);             //IN( , ,)
            $query="UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL  WHERE `email`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\"><strong>Recovered!</strong> Selected Data has been recovered successfully.</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Selected Data has not been recovered successfully. </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb22`.`email`  WHERE `email`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\"><strong>Recovered!</strong> Selected Data has been deleted successfully.</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\"><strong>Error!</strong> Selected Data has not been deleted successfully.</div>");
                Utility::redirect('index.php');

            }
        }

    }

}