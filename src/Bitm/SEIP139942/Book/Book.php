<?php
namespace App\Bitm\SEIP139942\Book;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;

class Book{
    public $title="";
    public $id="";
    public $conn;
    public $deleted_at;

    public function prepare($data="")
    {
        if(array_key_exists("title", $data))
        {
            $this->title=$data['title'];
        }
        if(array_key_exists("id", $data))
        {
            $this->id=$data['id'];
        }
      //echo $this;
    }
    public function __construct()
    {

        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("database connection failed");
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`Book` (`title`) VALUES ('".$this->title."')";

        $result=mysqli_query($this->conn,$query);
        if($result){
            //echo "Data Has been stored successfully";
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");

        }
        else {
            echo "Error";
        }
    }
    public function index(){
        $_allBook=array();
        $query="SELECT * FROM `book`WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;


    }
    public function view(){
        $query="SELECT * FROM `book` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        //$query="UPDATE `book` SET `title` = '".$this->title."' WHERE `book`.`id` = ".$this->id;
        $query="UPDATE `atomicprojectb22`.`book` SET `title` = '".$this->title."' WHERE `book`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            //echo "Data Has been stored successfully";
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");

        }
        else {
            echo "Error";
        }
    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
  </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb22`.`book` SET `deleted_at` = '".$this->deleted_at."' WHERE `book`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashedBook=array();
        $query="SELECT * FROM `book`WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashedBook[]=$row;
        }

        return $_trashedBook;


    }

    public function recover(){
        $query="UPDATE `atomicprojectb22`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectb22`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb22`.`book`  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
}


