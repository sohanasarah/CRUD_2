<?php
namespace App\Bitm\SEIP139942\Hobby;
use App\Bitm\SEIP139942\Book\Message;
use App\Bitm\SEIP139942\Book\Utility;
class Hobby{
    public $id;
    public $hobby;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die ("Database connection failed");
    }

    public function prepare($data="")
    {
        if(array_key_exists("hobby",$data))
        {
            $this->hobby=$data['hobby'];
        }
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectb22`.`hobby` (`hobbies`) VALUES ('".$this->hobby."')";

        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Your Hobbies had been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }


    public function index()
    {
        $_hobbies=array();
        $query="SELECT * FROM `hobby`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_hobbies[]=$row;
        }
        return $_hobbies;
    }

    public function view()
    {
        $query="SELECT * FROM `hobby` WHERE id=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectb22`.`hobby` SET `hobbies` = '".$this->hobby."' WHERE `hobby`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Updated!</strong> Your Hobby has been updated.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }

}