<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\Subscriber\Subscriber;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;

$mail= new Subscriber();
$allEmail=$mail->index();
//Utility::d($allBook);
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 align="center">Subscriber's List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create new</a>
    <a href="trashed_view.php" class="btn btn-danger" role="button">Trashed List</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Subscriber's Email</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allEmail as $mail){
                $sl++;
                ?>
                <td><?php echo $sl?></td>
                <td><?php echo $mail['id']?></td>
                <td><?php echo $mail['email']?></td>
                <td><a href="view.php?id=<?php echo $mail['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $mail['id'] ?>" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $mail['id'] ?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $mail['id'] ?>" class="btn btn-primary" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>

            </tbody>

        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>


</body>
</html>