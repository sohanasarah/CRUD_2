<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
//var_dump($_FILES);
$profile_picture= new ImageUploader();
$singleItem=$profile_picture->prepare($_POST)->view();
if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT'].'/CRUD_2/Resources/Images/'.$singleItem['images']);
    move_uploaded_file( $temporary_location,'../../../Resources/Images/'.$image_name);
    $_POST['image']=$image_name;

}


$profile_picture->prepare($_POST)->update();