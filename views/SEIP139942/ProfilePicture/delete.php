<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139942\Book\Utility;

$profile_picture= new ImageUploader();
$singleItem=$profile_picture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/CRUD_2/Resources/Images/'.$singleItem['images']);
$singleItem=$profile_picture->prepare($_GET)->delete();