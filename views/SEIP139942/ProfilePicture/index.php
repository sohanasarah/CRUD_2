<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;

$pro_pic= new ImageUploader();
$allInfo=$pro_pic->index();
//Utility::d($allBook);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Profile Picture</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
    <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
    <br>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allInfo as $info){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $info['id']?></td>
                <td><?php echo $info['name']?></td>
                <td><img src="../../../Resources/Images/<?php echo $info['images'] ?>" alt="image" height="100px"
                         width="100px" class="img-responsive"></td>
                <td><a href="view.php?id=<?php echo $info['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $info['id'] ?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $info['id'] ?>" class="btn btn-primary" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>
