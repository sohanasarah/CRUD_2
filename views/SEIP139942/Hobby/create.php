<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>Enter Hobbies</b></h2>
    <form role="form" action="store.php" method="post">
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Reading">Reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Football">Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cricket">Cricket</label>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</body>
</html>