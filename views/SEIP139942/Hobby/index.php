<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139942\Hobby\Hobby;
use App\Bitm\SEIP139942\Book\Utility;
use App\Bitm\SEIP139942\Book\Message;


$hobby=new Hobby();
$allHobbies=$hobby->index();


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>



<div class="container">
    <h2>List of Hobbies</h2>
    <a href="create.php" class="btn btn-info" role="button">Enter Hobbies</a>
    <div id="message">
        <?php echo Message::message();?>
    </div>
    <br><br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Hobbies</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allHobbies as $hobby){
                $sl++;
                ?>
                <td><?php echo $sl?></td>
                <td><?php echo $hobby['id']?></td>
                <td><?php echo $hobby['hobbies']?></td>
                <td><a href="view.php?id=<?php echo $hobby['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $hobby['id']?>" class="btn btn-info" role="button">Edit</a>
                    <a href="trash.php?id=<?php echo $hobby['id']?>" class="btn btn-primary" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger" role="button">Delete</a>
                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>